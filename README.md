# TS-VCD

### Technical Specification - Verification Control Document


TS-VCD is the first deliverable of the Fly Your Satellite! 3 programme. This 
document details all technical requirements for the AcubeSAT mission, including
all planned methods to verify these requirements at different programme stages.
A complementary document, called the supporting document is used to explain
details, including abbreviations and terms, to the reader.

You will find two documents here:

- The different **TS-VCD** versions, in .xlsx format
- The current version of the TS-VCD **Supporting Document** (SD) in PDF format

For readers planning to author their own technical specification, the following ECSS standards are recommended as a starting point.

- [ECSS-E-ST-10-06C – Technical requirements specification (6 March 2009)](https://ecss.nl/standard/ecss-e-st-10-06c-technical-requirements-specification/)
- [ECSS-E-ST-10-02C – Verification (6 March 2009)](https://ecss.nl/standard/ecss-e-st-10-02c-verification/)

#### Requirements Tree
The traceability of the requirements can be viewed in the interactive
Requirements Tree, available at:

https://helit.org/requirements-tree/

Please see the [`acubesat/utilities/requirements-tree` repo](https://gitlab.com/acubesat/utilities/requirements-tree)
for further information on the design of the tree, or if you want to further
implement it on another project.


## Disclaimer

The AcubeSAT project is carried out with the support of the Education Office of
the [European Space Agency](https://esa.int), under the educational
[Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/)
programme.

This repository has been authored by university students participating in the
AcubeSAT project. The views expressed herein by the authors can in no way be
taken to reflect the official opinion, or endorsement, of the European Space
Agency.
